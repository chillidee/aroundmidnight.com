<html>
<head>
<meta http-equiv="Content-Type" content="text/html" />
<title>Around Midnight</title>	
</head>

<style>
	@font-face {
	  font-family: 'Josefin Sans';
	  font-style: normal;
	  font-weight: 400;
	  src: local('Josefin Sans Regular'), local('JosefinSans-Regular'), url(https://fonts.gstatic.com/s/josefinsans/v12/Qw3aZQNVED7rKGKxtqIqX5EUDXx4Vn8sig.woff2) format('woff2');
	  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
	}
	@font-face {
	  font-family: 'Josefin Sans';
	  font-style: normal;
	  font-weight: 700;
	  src: local('Josefin Sans Bold'), local('JosefinSans-Bold'), url(https://fonts.gstatic.com/s/josefinsans/v12/Qw3FZQNVED7rKGKxtqIqX5Ectllte10hoJky_A.woff2) format('woff2');
	  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
	}
	@font-face {
	  font-family: 'Josefin Slab';
	  font-style: normal;
	  font-weight: 400;
	  src: local('Josefin Slab Regular'), local('JosefinSlab-Regular'), url(https://fonts.gstatic.com/s/josefinslab/v8/lW-5wjwOK3Ps5GSJlNNkMalnqg6vBMjoPg.woff2) format('woff2');
	  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
	}

	html,
	body {
		margin: 0 auto !important;
		padding: 0 !important;
		height: 100% !important;
		width: 100% !important;
	}
	
	* {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
	}
	
	table,
	td {
		mso-table-lspace: 0pt !important;
		mso-table-rspace: 0pt !important;
	}
	
	table {
		border-spacing: 0 !important;
		border-collapse: collapse !important;
		table-layout: fixed !important;
		margin: 0 auto !important;
	}
	
	table table table {
		table-layout: auto;
	}
	
	a {
            text-decoration: none;
	}
	
</style>

<body width="100%" style="margin: 0; mso-line-heigh-rule: exactly; background-color: #001D30;">

	<!-- Visually Hidden Preheader Text : BEGIN -->
	<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
		
	</div>
	<!-- Visually Hidden Preheader Text : END -->
	
	<!-- Top Nav : BEGIN -->
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #4D4D4F;">
		<tr>
			<td valign="top">
				<div style="max-width: 680px; margin: 0 auto;" class="email-container">
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td colspan="2" style="padding: 5px 20px; text-align: right; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 12px; line-height: 20px; color: #ffffff; text-transform: uppercase;">
								<a href="https://www.aroundmidnight.com/list-yourself/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Sign%20Up&utm_content=Header%20-%20Sign%20Up" style="text-decoration: none; color: inherit; margin-right: 20px">Sign Up</a>
								<a href="https://www.aroundmidnight.com/#signupnow/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Log%20In&utm_content=Header%20-%20Log%20In" style="text-decoration: none; color: inherit; margin-right: 20px">Log In</a>
								<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Explore&utm_content=Header%20-%20Explore" style="text-decoration: none; color: inherit; margin-right: 20px">Explore</a>
								<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Blog&utm_content=Header%20-%20Blog" style="text-decoration: none; color: inherit; margin-right: 20px; display: none;">Blog</a>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<!-- Top Nav : END -->
	
	<div style="max-width: 680px; margin: 0 auto;">

	<!-- Header Image : START -->
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto; border-collapse: collapse;">
		<tr>
			<td colspan="2" style="background-color: #001d30; text-align: center;">
				<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Logo"><img src="https://www.aroundmidnight.com/wp-content/uploads/2018/05/AM-Logo-Horizontal-Colour-PNG.png" alt="Around Midnight Logo" style="height: 200px; font-family: Josefin Sans, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 30px; text-align: center; color: #D70075;"></a>
			</td>
		</tr>
	<!-- Header Image : END -->
		
		<!-- Visually Hidden Preheader Text : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
			
		</div>
		<!-- Visually Hidden Preheader Text : END -->

		<!-- Header Content : START -->
		<tr>
			<td colspan="2" align="center">
				<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Invitation&utm_content=Main%20Image"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Launch.png" alt="Condom Launching Upwards" width="60%" style="margin: -20px 0 0 0; max-width: 680px; font-family: Josefin Sans, Helvetica Neue, Helvetica, Arial, sans-serif; text-align: center; font-size: 30px; color: #FFFFFF; width:60%;"></a>
			</td>
		</tr>
		<!-- End Header -->

		<!-- Body -->
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #D70075; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; text-transform: uppercase;">
				<h1 style="margin-bottom: -20px">We're Launching in NZ!</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center;">
				Around Midnight is a location-based search and discovery platform that helps its customers quickly &amp; easily look for the closest adult venues and services.<br><br>
				We invite you to have a look at what Around Midnight is about and how we are revolutionising the industry.<br><br>
				We are also offering you a free listing - start listing now before we launch!
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 10px 20px; text-align: center;">
				<!-- Button : BEGIN -->
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center">
						<tr>
							<td class="button-td button-td-primary" style="border-radius: 4px; background: #D70075;">
								<a class="button-a button-a-primary" href="https://www.aroundmidnight.com/#signupnow/?utm_source=WP&utm_medium=Email&utm_campaign=Invitation&utm_content=Visit%20Button%201" style="background: #D70075; border: 1px solid #D70075; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 18px; line-height: 15px; text-decoration: none; text-transform: uppercase; padding: 15px 20px; color: #ffffff; display: block; border-radius: 4px;">Visit Around Midnight now</a>
							</td>
						</tr>
					</table>
				<!-- Button : END -->
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<hr style="border-color: #D70075; border-style: inset; margin: 20px 0;">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto; border-collapse: collapse;">
					<tr>
						<td style="background-color: #001D30; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: left;">
							<span style="color: #D70075; text-transform: uppercase; font-size: 24">Put Your Business on the Map</span>
							<p>Customers quickly &amp; easily search for your business or services on our platform.</p>
						</td>
						<td>
							<img src="https://www.aroundmidnight.com/wp-content/uploads/2018/06/Explore-Listings.png" width="90%" style="width: 90%">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<hr style="border-color: #D70075; border-style: inset; margin: 20px 0;">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto; border-collapse: collapse;">
					<tr>
						<td>
							<img src="https://www.aroundmidnight.com/wp-content/uploads/elementor/thumbs/desktop-mobile-nrvj9v7o1raoafxhl3ci3k143w2x0f7oob7o8hkz5s.png" width="90%" style="width: 90%" alt="Around Midnight Displayed on Multiple Devices">
						</td>
						<td style="background-color: #001D30; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: left;">
							<span style="color: #D70075; text-transform: uppercase; font-size: 24">Multiple Device Support</span>
							<p>Our platform is available across multiple devices - desktop, tablets and mobile phones.</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<hr style="border-color: #D70075; border-style: inset; margin: 20px 0;">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto; color: #712C86">
					<tr>
						<td colspan="2" style=" color: #D70075; font-size: 25px; padding: 10px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; text-transform: uppercase">
							Listed so far...
						</td>
					</tr>
					<tr>
						<td>
							<div style="background-color: #712C86; color: #D70075; font-size: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; border-radius: 10px; width: 80%; padding: 20px"><span style="color: #D70075; text-transform: uppercase; font-size: 60px">74</span><br>
                            <span style="color: #FFFFFF; text-transform: uppercase; font-size: 24px">ESCORTS</span></div>
						</td>
						<td>
							<div style="background-color: #712C86; color: #D70075; font-size: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; border-radius: 10px; width: 80%; padding: 20px">
								<span style="color: #D70075; text-transform: uppercase; font-size: 60px">91</span><br>
								<span style="color: #FFFFFF; text-transform: uppercase; font-size: 24px">BUSINESSES</span></td>
							</div>
					</tr>
					<tr>
						<td colspan="2" style=" color: #D70075; font-size: 25px; padding: 10px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; text-transform: uppercase">
							... and counting.
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<hr style="border-color: #D70075; border-style: inset; margin: 30px 0;">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto; border-collapse: collapse;">
					<tr>
						<td style="background-color: #001D30; font-size: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center;">
							<span style="color: #FFFFFF; text-transform: uppercase; font-size: 24">Find out more</span>
						</td>
						<td class="button-td button-td-primary" style="border-radius: 4px; background: #D70075;">
							<a class="button-a button-a-primary" href="https://www.aroundmidnight.com/#signupnow/?utm_source=WP&utm_medium=Email&utm_campaign=Invitation&utm_content=Visit%20Button%202" style="background: #D70075; border: 1px solid #D70075; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 18px; line-height: 15px; text-decoration: none; text-transform: uppercase; padding: 15px 20px; color: #ffffff; display: block; border-radius: 4px;">Visit Around Midnight now</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- End Body -->
		
		<!-- Clear Spacer : BEGIN -->
	   	<tr>
			<td aria-hidden="true" height="40px" style="font-size: 0px; line-height: 0px;">&nbsp;
				
			</td>
		</tr>
		<!-- Clear Spacer : END -->

		<!-- Footer -->
		<tr>
			<td colspan="2" style="padding: 15px 20px; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 14px; line-height: 20px; color: #FFFFFF; text-align: center">
				<p style="margin: 0;">To opt out, <a href="mailto:contact@aroundmidnight.com?subject=Unsubscribe | Around Midnight&body=Hi Team at Around Midnight, I'd like to unsubscribe. My reasons being:" style="text-decoration: underline; color: #ffffff">reply to this email</a> and we'd really appreciate if you could tell us why you're not interested.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #712c86; border-radius: 0 0  0 10px; padding: 10px 20px; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 14px; color: #FFFFFF;">
				<p style="margin: 0;">Around Midnight &copy; 2018. All rights reserved.</p>
			</td>
			<td style="background-color: #712c86; border-radius: 0 0 10px 0; padding: 10px 20px; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 14px; color: #FFFFFF; text-align: right;">
				<p style="margin: 0; text-decoration: none;">
					<a href="https://www.facebook.com/ardmdnt"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Facebook.png" alt="Facebook" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
					<a href="https://www.twitter.com/ardmdnt"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Twitter.png" alt="Twitter" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
					<a href="https://www.instagram.com/ardmdnt"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Instagram.png" alt="Instagram" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
					<a href="https://ardmdnt.tumblr.com/"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Tumblr.png" alt="Tumblr" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
				</p>
			</td>
		</tr>
		<!-- End Footer -->
		
	</table>
	</div>
</body>
</html>