<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>


		<!-- Visually Hidden Preheader Text : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
			Use the link below to reset your password. If this wasn't you, you may safely ignore this email.
		</div>
		<!-- Visually Hidden Preheader Text : END -->

		<!-- Header Content : START -->
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #FFFFFF; font-size: 25px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center">
				<h1>Forgot Your Password?</h1>
			</td>
		</tr>
		<!-- End Header -->

		<!-- Body -->
		<tr>
			<td colspan="2" style="background-color: #ffffff; border-radius: 10px 10px 0 0;">
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 20px; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; line-height: 20px; color: #4D4D4F;" colspan="2">
							<h1 style="margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #D70075; font-weight: normal; margin-bottom: 20px">That's okay, it happens.</h1>
							<p style="margin: 0 0 10px; font-size: 17px">
								Use the link below to reset your password.<br>
								If this wasn't you, you may safely ignore this email.<br><br>									
							</p>
						</td>
					</tr>
					<tr>
						<td style="padding: 0 20px 20px;" colspan="2">
							<!-- Button : BEGIN -->
								<table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
									<tr>
										<td class="button-td button-td-primary" style="border-radius: 4px; background: #001D30;">
											<a class="button-a button-a-primary" href="<?php echo esc_url(str_replace('my-account/lost-password/?', 'wp-login.php?action=rp&', add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) )) ); ?>" style="background: #D70075; border: 1px solid #D70075; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; text-transform: uppercase; padding: 13px 17px; color: #ffffff; display: block; border-radius: 4px;">Update Password</a>
										</td>
									</tr>
								</table>
							<!-- Button : END -->
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding: 20px; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 14px;">
							If the button above doesn't work, please use the following link below:<br>
							<a href=<?php echo str_replace('my-account/lost-password/?', 'wp-login.php?action=rp&', esc_url( add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) )); ?> style="font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: small; text-decoration: none; color: #4D4D4F;"> <?php echo esc_url(str_replace('my-account/lost-password/?', 'wp-login.php?action=rp&', add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) )); ?> </a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- End Body -->

<?php do_action( 'woocommerce_email_footer', $email ); ?>