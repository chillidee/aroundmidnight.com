		<!-- Visually Hidden Preheader Text : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
			Hi {{UserName}}, Congratulations! Your listing has been approved. View your listing by following the link below.
		</div>
		<!-- Visually Hidden Preheader Text : END -->

		<!-- Header Content : START -->
		<tr>
			<td style="background-color: #001D30; color: #FFFFFF; font-size: 25px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center">
				<img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/06/profile-disapproved.png" width="310" height="auto" border="0" alt="alt_text" class="center-on-narrow" style="width: 100%; max-width: 310px; height: auto; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; line-height: 20px; color: #4D4D4F;">
			</td>
			<td style="background-color: #001D30; color: #FFFFFF; font-size: 25px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: left">
				<h1>Your Listing Is Not Approved</h1>
			</td>
		</tr>
		<!-- End Header -->

		<!-- Body -->
		<tr>
			<td colspan="2" style="background-color: #ffffff; border-radius: 10px 10px 0 0;">
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 20px; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; line-height: 20px; color: #4D4D4F;" colspan="2">
							<h1 style="margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #D70075; font-weight: normal; margin-bottom: 20px">Hi {{UserName}}</h1>
							<p style="margin: 0 0 10px; font-size: 17px">
								Congratulations!<br><br>
								Your listing seems to follow our guidelines and has been approved!<br>
								View your listing by following the link below now.									
							</p>
						</td>
					</tr>
					<tr>
						<td style="padding: 0 20px 20px;" colspan="2">
							<!-- Button : BEGIN -->
								<table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
									<tr>
										<td class="button-td button-td-primary" style="border-radius: 4px; background: #001D30;">
											<a class="button-a button-a-primary" href="{{UrlActivationCode}}" style="background: #D70075; border: 1px solid #D70075; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; text-transform: uppercase; padding: 13px 17px; color: #ffffff; display: block; border-radius: 4px;">View Your Listing</a>
										</td>
									</tr>
								</table>
							<!-- Button : END -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- End Body -->