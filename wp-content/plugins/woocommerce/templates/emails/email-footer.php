<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

	<!-- Footer -->
		<tr>
			<td style="background-color: #712c86; border-radius: 0 0  0 10px; padding: 15px 20px; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 14px; line-height: 20px; color: #FFFFFF;">
				<p style="margin: 0;">Around Midnight &copy; 2020. All rights reserved.</p>
			</td>
			<td style="background-color: #712c86; border-radius: 0 0 10px 0; padding: 15px 20px; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 14px; line-height: 20px; color: #FFFFFF; text-align: right;">
				<p style="margin: 0; text-decoration: none;">
					<a href="https://www.facebook.com/ardmdnt"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Facebook.png" alt="Facebook" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
					<a href="https://www.twitter.com/ardmdnt"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Twitter.png" alt="Twitter" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
					<a href="https://www.instagram.com/ardmdnt"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Instagram.png" alt="Instagram" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
					<a href="https://ardmdnt.tumblr.com/"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Tumblr.png" alt="Tumblr" height="25px" style="background-color: #712c86; height: 25px; border-radius: 4px; margin-right: 5px"></a>
				</p>
			</td>
		</tr>
		<!-- End Footer -->
		
		<!-- Clear Spacer : BEGIN -->
	   <tr>
			<td aria-hidden="true" height="50px" style="font-size: 0px; line-height: 0px;">&nbsp;

			</td>
		</tr>
		<!-- Clear Spacer : END -->