<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<!doctype html>

<html <?php language_attributes() ?> >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo ( 'charset' ); ?>" />
<title>Around Midnight</title>	
</head>

<body width="100%" style="margin: 0; mso-line-heigh-rule: exactly; background-color: #001D30;">

	<!-- Top Nav : BEGIN --><!--
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #4D4D4F;">
		<tr>
			<td valign="top">
				<div style="max-width: 680px; margin: 0 auto;" class="email-container">
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td colspan="2" style="padding: 5px 20px; text-align: right; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 12px; line-height: 20px; color: #ffffff; text-transform: uppercase;">
								<a href="https://www.aroundmidnight.com/list-yourself/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Sign%20Up" style="text-decoration: none; color: inherit; margin-right: 20px">Sign Up</a>
								<a href="https://www.aroundmidnight.com/#signupnow/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Log%20In" style="text-decoration: none; color: inherit; margin-right: 20px">Log In</a>
								<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Explore" style="text-decoration: none; color: inherit; margin-right: 20px">Explore</a>
								<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Blog" style="text-decoration: none; color: inherit; margin-right: 20px; display: none;">Blog</a>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>-->
	<!-- Top Nav : END -->
	
	<div style="max-width: 680px; margin: 0 auto;">
		
	<!-- Visually Hidden Preheader Text : BEGIN -->
	<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
		
	</div>
	<!-- Visually Hidden Preheader Text : END -->
	
	<!-- Top Nav : BEGIN -->
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #4D4D4F;">
		<tr>
			<td valign="top">
				<div style="max-width: 680px; margin: 0 auto;" class="email-container">
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td colspan="2" style="padding: 5px 20px; text-align: right; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 12px; line-height: 20px; color: #ffffff; text-transform: uppercase;">
								<a href="https://www.aroundmidnight.com/list-yourself/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Sign%20Up" style="text-decoration: none; color: inherit; margin-right: 20px">Sign Up</a>
								<a href="https://www.aroundmidnight.com/#signupnow/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Log%20In" style="text-decoration: none; color: inherit; margin-right: 20px">Log In</a>
								<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Explore" style="text-decoration: none; color: inherit; margin-right: 20px">Explore</a>
								<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Blog" style="text-decoration: none; color: inherit; margin-right: 20px; display: none;">Blog</a>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<!-- Top Nav : END -->
	
	<div style="max-width: 680px; margin: 0 auto;">

	<!-- Header Image : START -->
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto; border-collapse: collapse;">
		<tr>
			<td colspan="2" style="background-color: #001d30; text-align: center;">
				<a href="https://www.aroundmidnight.com/?utm_source=WP&utm_medium=Email&utm_campaign=Header%20Link&utm_term=Logo"><img src="https://www.aroundmidnight.com/wp-content/uploads/2018/05/AM-Logo-Horizontal-Colour-PNG.png" alt="Around Midnight Logo" style="height: 200px; font-family: Josefin Sans, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 30px; text-align: center; color: #D70075"></a>
			</td>
		</tr>
	<!-- Header Image : END -->