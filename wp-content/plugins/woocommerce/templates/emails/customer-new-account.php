		<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

		<!-- Body -->
		<tr>
			<td colspan="2" id="body">
				<p>Hi <?php printf( __( '%1$s', 'woocommerce' ), esc_html( $blogname ),  esc_html( $user_login ) ); ?>,</p>
				<p>Welcome to Around Midnight.<br>
				Log in below to update your details.</p>
			</td>
		</tr>

		<tr>
			<td colspan="2">
				<div>
					<!--[if mso]>
  						<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.aroundmidnight.com" style="height:50px;v-text-anchor:middle;width:250px;" arcsize="6%" stroke="f" fillcolor="#712c86">
						<w:anchorlock/>
						<center>
					<![endif]-->
					
					<a href="https://www.aroundmidnight.com" style="background-color:#712c86;border-radius:3px;color:#ffffff;display:inline-block;font-size:20px;font-weight:bold;line-height:50px;text-align:center;text-decoration:none;width:250px;-webkit-text-size-adjust:none;margin: 10px 0px 20px 0px;">LOG IN NOW</a>

					<!--[if mso]>
						</center>
				 		</v:roundrect>
					<![endif]-->
					
				</div>
			</td>
		</tr>
		<!-- End Body -->

		<?php do_action( 'woocommerce_email_footer', $email );