<!-- Visually Hidden Preheader Text : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
			
		</div>
		<!-- Visually Hidden Preheader Text : END -->

		<!-- Header Content : START -->
		<tr>
			<td colspan="2" align="center">
				<a href="https://www.aroundmidnight.com/"><img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/07/Launch.png" alt="Condom Launching Upwards" width="100%" style="margin: -20px 0 0 0; max-width: 680px; font-family: Josefin Sans, Helvetica Neue, Helvetica, Arial, sans-serif; text-align: center; font-size: 30px; color: #FFFFFF; width:60%;"></a>
			</td>
		</tr>
		<!-- End Header -->

		<!-- Body -->
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #D70075; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; text-transform: uppercase;">
				<h1 style="margin-bottom: -20px">We've Launched!</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center;">
				Around Midnight is a location-based search and discovery platform that allows its customers to look for the closest adult venues and services.<br><br>
				Come have a look at what Around Midnight is about and how we are revolutionising the industry.<br><br>
				We've partnered with MyPlaymate and Brothels, and we've automagically listed your business on our new site.<br><br>
				You may use the following login details to update your listing:
			</td>
		</tr>
		<tr>
			<td style="background-color: #D70075; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; border-radius: 4px 0 0 4px">
				Username:<br>
				<p style="color: #712C86 !important; font-size: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; text-decoration: none !important;"><span style="text-decoration: none;">{{UserName}}</span></p>
			</td>
			<td style="background-color: #D70075; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; border-radius: 0 4px 4px 0">
				Password:<br>
				<p style="color: #712C86; font-size: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; text-decoration: none;">{{Password}}</p>
			</td>
		</tr>
	   	<tr>
			<td aria-hidden="true" height="20px" style="font-size: 0px; line-height: 0px;">&nbsp;
				
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 10px 20px; text-align: center;">
				<!-- Button : BEGIN -->
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center">
						<tr>
							<td class="button-td button-td-primary" style="border-radius: 4px; background: #D70075;">
								<a class="button-a button-a-primary" href="https://www.aroundmidnight.com/#signupnow" style="background: #D70075; border: 1px solid #D70075; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 18px; line-height: 15px; text-decoration: none; text-transform: uppercase; padding: 15px 20px; color: #ffffff; display: block; border-radius: 4px;">Visit Around Midnight now</a>
							</td>
						</tr>
					</table>
				<!-- Button : END -->
			</td>
		</tr>
		<!-- End Body -->
		
		<!-- Clear Spacer : BEGIN -->
	   <tr>
			<td aria-hidden="true" height="20px" style="font-size: 0px; line-height: 0px;">&nbsp;
				
			</td>
		</tr>
		<!-- Clear Spacer : END -->