<html>
<head>
<meta http-equiv="Content-Type" content="text/html" />
<title>Around Midnight</title>	
</head>

<body width="100%" style="margin: 0; mso-line-heigh-rule: exactly; background-color: #001D30;">

	<!-- Visually Hidden Preheader Text : BEGIN -->
	<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
		
	</div>
	<!-- Visually Hidden Preheader Text : END -->
	
	<!-- Top Nav : BEGIN -->
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #4D4D4F;">
		<tr>
			<td valign="top">
				<div style="max-width: 680px; margin: 0 auto;" class="email-container">
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="display:none">
						<tr>
							<td style="padding: 5px 20px; text-align: left; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 12px; line-height: 20px; color: #ffffff; text-transform: uppercase">
								<webversion style="text-decoration: none; font-weight: bold;">View as a Web Page</webversion>
							</td>
							<td style="padding: 5px 20px; text-align: right; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 12px; line-height: 20px; color: #ffffff; text-transform: uppercase;">
								<a href="#" style="text-decoration: none; color: inherit; margin-right: 20px">Log In</a>
								<a href="#" style="text-decoration: none; color: inherit; margin-right: 20px">Explore</a>
								<a href="#" style="text-decoration: none; color: inherit; margin-right: 20px">Blog</a>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<!-- Top Nav : END -->
	
	<div style="max-width: 680px; margin: 0 auto;">

	<!-- Header Image : START -->
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0 auto; border-collapse: collapse;">
		<tr>
			<td colspan="2" style="background-color: #001d30; text-align: center;">
				<a href="https://www.aroundmidnight.com/biz"><img src="https://www.aroundmidnight.com/wp-content/uploads/2018/05/AM-Logo-Horizontal-Colour-PNG.png" alt="Around Midnight Logo" width="350" style="max-width: 350px; font-family: Josefin Sans, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 30px; text-align: center; color: #D70075"></a>
			</td>
		</tr>
	<!-- Header Image : END -->
		
			<!-- Visually Hidden Preheader Text : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
			
		</div>
		<!-- Visually Hidden Preheader Text : END -->

		<!-- Header Content : START -->
		<tr>
			<td colspan="2" align="center">
				<a href="https://www.aroundmidnight.com/biz"><img src="https://www.aroundmidnight.com/wp-content/uploads/2018/06/desktop-mobile.png" alt="Around Midnight website loaded on a browser, tablet and mobile phone" width="650" style="margin: -20px 0 -50px 0; max-width: 650px; width: 650px; font-family: Josefin Sans, Helvetica Neue, Helvetica, Arial, sans-serif; text-align: center; font-size: 30px; color: #FFFFFF;"></a>
			</td>
		</tr>
		<!-- End Header -->

		<!-- Body -->
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #D70075; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center; text-transform: uppercase;">
				<h1 style="margin-bottom: -20px">Put Yourself on the Map with Our Revolutionary Platform</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center;">
				Around Midnight is the fastest-growing location-based platform for the sex industry.<br><br>
				Create your free listing now.
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="50%">
					<tr>
						<td style="border-radius: 4px; background: #D70075; text-align: center">
							<a href="https://www.aroundmidnight.com/biz#signupnow" style="background: #D70075; border: 1px solid #D70075; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 18px; line-height: 15px; text-decoration: none; text-transform: uppercase; padding: 15px 20px; color: #ffffff; display: block; border-radius: 4px; text-align: center;">List Yourself for Free</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #FFFFFF; font-size: 20px; padding: 20px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: center;">
				Or visit <a href="https://www.aroundmidnight.com/biz" style="text-decoration: none; color: #D70075;">AroundMidnight.com</a> and see what we have to offer
			</td>
		</tr>
		<!-- End Body -->
		
		<!-- Clear Spacer : BEGIN -->
	   <tr>
			<td aria-hidden="true" height="20px" style="font-size: 0px; line-height: 0px;">&nbsp;
				
			</td>
		</tr>
		<!-- Clear Spacer : END -->

		<!-- Footer -->
		<tr>
			<td colspan="2" style="padding: 15px 20px; font-family: 'Josefin Slab', 'Roboto Slab', sans-serif; font-size: 14px; line-height: 20px; color: #FFFFFF; text-align: center">
				<p style="margin: 0;">A member of the Adult Press Group, Brothels.com.au, and MyPlaymate</p>
				<p style="margin: 0;">Around Midnight &copy; 2018. All rights reserved.</p>
			</td>
		</tr>
		<!-- End Footer -->
		
	</table>
	</div>
</body>
</html>