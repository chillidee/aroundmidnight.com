		<!-- Visually Hidden Preheader Text : BEGIN -->
		<div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif;">
			Hi {{UserName}}, Welcome to Around Midnight - a free location-based search and discovery platform that helps customers discover the nearest adult entertainment venues and services.
		</div>
		<!-- Visually Hidden Preheader Text : END -->

		<!-- Header Content : START -->
		<tr>
			<td colspan="2" style="background-color: #001D30; color: #FFFFFF; font-size: 25px; font-family: 'Comfortaa', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; text-align: left; vertical-align: middle; border-radius: 10px 10px 0 0">
				<img src="https://www.aroundmidnight.com/wp-content/uploads/2018/06/Map-Preview-2.jpg" width="100%" style="width: 100%; border-radius: 10px 10px 0 0"
			</td>
		</tr>
		<!-- End Header -->

		<!-- Body -->
		<tr>
			<td colspan="2" style="background-color: #ffffff;">
				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 20px; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; color: #4D4D4F; margin-top: 0;" colspan="2">
							<h1 style="font-size: 25px; color: #D70075; font-weight: normal; margin-bottom: -20px;">
								A Gift from the Team at Around Midnight
							</h1>
						</td>
					</tr>
					<tr>
						<td style="padding: 20px; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; color: #4D4D4F; vertical-align: top">
							<h2 style="font-size: 20px; color: #712C86; font-weight: normal; margin-bottom: 5px">
								What is Around Midnight?
							</h2>
							<p style="font-size: 14px; line-height: 25px;">
								Around Midnight is a location-based search and discovery platform that helps customers discover the nearest adult entertainment venues and services.
							</p>
						</td>
						<td style="padding: 20px; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; color: #4D4D4F; vertical-align: top">
							<h2 style="font-size: 20px; color: #712C86; font-weight: normal; margin-bottom: 5px">
								You've Been Selected!
							</h2>
							<p style="font-size: 14px; line-height: 25px;">
								We've carefully selected you to be one of the first in the industry to experience our revolutionary platform. Come give it a taste before everyone else gets their hands on it.
							</p>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding: 20px; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; color: #4D4D4F; vertical-align: top; line-height: 20px">
							<p> 
								Don't get left behind! Head on over to <a href="http://bit.ly/around-midnight-prelaunch" style="text-decoration: none; color: #D70075;">Around Midnight</a> now to find out more and start <a href="http://bit.ly/around-midnight-prelaunch" style="text-decoration: none; color: #D70075;">creating your free listing!</a>
								<img src="https://www.aroundmidnight.com/wp-content/uploads/2018/06/email-tracking-prelaunch.png">
							</p>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding: 20px; vertical-align: middle;">
							<!-- Button : BEGIN -->
								<table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
									<tr>
										<td class="button-td button-td-primary" style="border-radius: 4px; background: #D70075;">
											<a class="button-a button-a-primary" href="http://bit.ly/around-midnight-prelaunch" style="background: #D70075; border: 1px solid #D70075; font-family: 'Josefin Sans', 'Helvetica Neue', 'Helvetica', Arial, sans-serif; font-size: 15px; line-height: 15px; text-decoration: none; text-transform: uppercase; padding: 13px 17px; color: #ffffff; display: block; border-radius: 4px;">Create Your Free Listing</a>
										</td>
									</tr>
								</table>
							<!-- Button : END -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- End Body -->