<?php
    $data = c27()->merge_options([
            'skin' => 'transparent',
            'ids' => '',
            'align' => 'center',
        ], $data);

    $category_ids = (array) explode( ',', (string) $data['ids'] );

    $categories = (array) get_terms([
        'taxonomy' => 'job_listing_category',
        'hide_empty' => false,
        'include' => array_filter( array_map( 'absint', $category_ids ) ) ? : [-1],
        'orderby' => 'title',
        ]);

    if ( is_wp_error( $categories ) ) {
        return false;
    }
 ?>

<div class="featured-categories <?php echo esc_attr( $data['skin'] ) ?>" style="display: inline-block;">
	<ul>
		<?php foreach ( $categories as $category):
			$term = new CASE27\Classes\Term( $category );
			$image = $term->get_image();
			?>
			<li class="reveal text-center">					
				<a href="explore/?search_location=&job_category%5B%5D=<?php echo $term->get_slug() ?>&tab=search-form&type=place" 
				onclick="trackQuickies('<?php echo $term->get_name() ?>'); searchCurrentPositionByCategory('<?php echo $term->get_slug() ?>');" 
				style="<?php echo $image && is_array($image) ? "background-image: url('" . esc_url( $image['sizes']['large'] ) . "'); background-repeat: no-repeat; background-position: center; background-size: 100% 100%;" : ''; ?>">
					<div class="slc-icon">
						<i class="<?php echo esc_attr( $term->get_icon() ) ?>"></i>
					</div>
					<div class="slc-info">
						<p><?php echo esc_html( $term->get_name() ) ?></p>							
					</div>
				</a>
			</li>				
		<?php endforeach ?>
	</ul>
</div>

<script type="text/javascript">
    
	function searchCurrentPositionByCategory(category) {	
		var location = '';	
		
		navigator.geolocation.watchPosition(function(position) {
		  navigator.geolocation.getCurrentPosition(function(e) {
				var t = new google.maps.LatLng(e.coords.latitude, e.coords.longitude);
				(new google.maps.Geocoder).geocode({
					latLng: t
				}, function(e, t) {
					if("OK" === t)						
						e[1] ? location = e[1].formatted_address : location = e[0].formatted_address					
					redirect('explore/?search_location=' + esc_url_js(location) + '&job_category%5B%5D=' + category + '&tab=search-form&type=place')					
				})
			})
		},
		function (error) { 
		  if (error.code == error.PERMISSION_DENIED)
			  redirect('explore/?search_location=&job_category%5B%5D=' + category + '&tab=search-form&type=place');
		});
	}
	
	function esc_url_js(str) {
		return str.replace(/ /g, '+').replace(/,/g,'%2C');
	}
	
	function redirect (url) {
		var ua        = navigator.userAgent.toLowerCase(),
			isIE      = ua.indexOf('msie') !== -1,
			version   = parseInt(ua.substr(4, 2), 10);

		// Internet Explorer 8 and lower
		if (isIE && version < 9) {
			var link = document.createElement('a');
			link.href = url;
			document.body.appendChild(link);
			link.click();
		}

		// All other browsers can use the standard window.location.href (they don't lose HTTP_REFERER like Internet Explorer 8 & lower does)
		else { 
			window.location.href = url; 
		}
	}
	
</script>
