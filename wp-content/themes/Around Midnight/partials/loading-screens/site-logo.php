<div class="loader-bg main-loader site-logo-loader" style="background-color: <?php echo esc_attr( c27()->get_setting( 'general_loading_overlay_background_color' ,'#ffffff' ) ) ?>;">
	<img src="https://staging.aroundmidnight.com/wp-content/uploads/2018/05/Loading-Symbol.gif">
</div>