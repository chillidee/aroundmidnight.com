<?php
    $data = c27()->merge_options([
            'listing' => '',
            'options' => [],
            'wrap_in' => '',
        ], $data);

    if ( ! class_exists( 'WP_Job_Manager' ) ) {
        return false;
    }

    if ( ! $data['listing'] ) {
        return false;
    }

    $listing = new CASE27\Classes\Listing( $data['listing'] );

    if ( ! $listing->type ) {
        return;
    }

    $options = $listing->get_preview_options();

    $options = c27()->merge_options( $options, (array) $data['options'] );

    $classes = [
        'default' => '',
        'alternate' => 'lf-type-2'
    ];

    $categories = $listing->get_field( 'job_category' );

    $first_category = $categories ? new CASE27\Classes\Term( $categories[0] ) : false;
    $listing_thumbnail = job_manager_get_resized_image( $listing->get_field('job_logo'), 'thumbnail' );
    $latitude = false;
    $longitude = false;
	
	$suburb = $listing->get_data('geolocation_city');
	
	if(!isset($suburb) || empty($suburb) || strlen($suburb) < 2) {
		$suburb	= get_post_meta($listing->get_id(), '_job_location', true);	
		$suburb = str_ireplace(', ' . $listing->get_data('geolocation_state_short'), '', $suburb);	
		$suburb = str_ireplace($listing->get_data('geolocation_country_long'), '', $suburb);		
		$suburb = str_ireplace($listing->get_data('geolocation_street'), '', $suburb);
		$suburb = str_ireplace($listing->get_data('geolocation_street_number'), '', $suburb);				
		$suburb = str_replace(',', '', $suburb);
		$suburb = trim($suburb);		
	}
	
	$suburb = ucfirst($suburb);

    if ( is_numeric( $listing->get_data('geolocation_lat') ) ) {
        $latitude = $listing->get_data('geolocation_lat') + ( rand(0, 1000) / 10e6 );
    }

    if ( is_numeric( $listing->get_data('geolocation_long') ) ) {
        $longitude = $listing->get_data('geolocation_long') + ( rand( 0, 1000 ) / 10e6 );
    }

    $data['listing']->_c27_marker_data = [
        'lat' => $latitude,
        'lng' => $longitude,
        'thumbnail' => $listing_thumbnail,
        'category_icon' => $first_category ? $first_category->get_icon() : null,
        'category_color' => $first_category ? $first_category->get_color() : null,
        'category_text_color' => $first_category ? $first_category->get_text_color() : null,
    ];

    $detailsCount = 0;
    foreach ((array) $options['footer']['sections'] as $section) {
        if ( $section['type'] == 'details' ) $detailsCount = count( $section['details'] );
    }

    if ( ! isset( $data['listing']->_c27_show_promoted_badge ) ) {
        $data['listing']->_c27_show_promoted_badge = true;
    }

    $isPromoted = false;
    if (
         $data['listing']->_c27_show_promoted_badge &&
         $listing->get_data( '_case27_listing_promotion_start_date' ) &&
         strtotime( $listing->get_data( '_case27_listing_promotion_start_date' ) ) &&
         $listing->get_data( '_case27_listing_promotion_end_date' ) &&
         strtotime( $listing->get_data( '_case27_listing_promotion_end_date' ) )
     ) {
        try {
            $startDate = new DateTime( $listing->get_data( '_case27_listing_promotion_start_date' ) );
            $endDate = new DateTime( $listing->get_data( '_case27_listing_promotion_end_date' ) );
            $currentDate = new DateTime( date( 'Y-m-d H:i:s' ) );

            if ( $currentDate >= $startDate && $currentDate <= $endDate ) {
                $isPromoted = true;
            }
        } catch (Exception $e) {}
    }
?>

<div class="<?php echo $data['wrap_in'] ? esc_attr( $data['wrap_in'] ) : '' ?>">
<div <?php job_listing_class( esc_attr( " lf-item-container listing-preview {$classes[$options['template']]} type-{$listing->type->get_slug()} " . ($detailsCount > 2 ? ' lf-small-height ' : '') ), $listing->get_id()); ?>
     data-id="listing-id-<?php echo esc_attr( $listing->get_id() ); ?>"
     data-latitude="<?php echo esc_attr( $latitude ); ?>"
     data-longitude="<?php echo esc_attr( $longitude ); ?>"
     data-category-icon="<?php echo esc_attr( $first_category ? $first_category->get_icon() : '' ) ?>"
     data-category-color="<?php echo esc_attr( $first_category ? $first_category->get_color() : '' ) ?>"
     data-category-text-color="<?php echo esc_attr( $first_category ? $first_category->get_text_color() : '' ) ?>"
     data-thumbnail="<?php echo esc_url( $listing_thumbnail ) ?>"
     >
    <div class="lf-item">
        <a target='_blank' href="<?php echo esc_url( $listing->get_link() ) ?>" onclick="trackListing(this, '<?php echo $listing->get_name() ?>');">
            <div class="overlay" style="
                background-color: <?php echo esc_attr( c27()->get_setting('listing_preview_overlay_color', '#242429') ); ?>;
                opacity: <?php echo esc_attr( c27()->get_setting('listing_preview_overlay_opacity', '0.5') ); ?>;
                "></div>

            <?php if ($options['background']['type'] == 'image' && ( $cover = job_manager_get_resized_image( $listing->get_field( 'job_logo' ) , 'large' ) ) ): ?>
                <div
                    class="lf-background"
                    style="background-image: url('<?php echo esc_url( $cover ) ?>');">
                </div>
            <?php endif ?>

            <?php if ($options['template'] == 'alternate'): ?>
                <div class="lf-item-info-2">
                    <?php if ( $logo = $listing->get_field( 'job_logo' ) ): ?>
                        <div
                            class="lf-avatar"
                            style="background-image: url('<?php echo esc_url( job_manager_get_resized_image( $logo, 'thumbnail') ) ?>')">
                        </div>
                    <?php endif ?>

                    <h4><?php echo apply_filters( 'the_title', $listing->get_name(), $listing->get_id() ) ?></h4>

                    <?php if ( $tagline = $listing->get_field('job_tagline') ): ?>
                        <h6><?php echo esc_html( $tagline ) ?></h6>
                    <?php elseif ( $description = $listing->get_field('job_description') ): ?>
                        <h6><?php echo c27()->the_text_excerpt( $description, 114 ) ?></h6>
                    <?php endif ?>

                    <?php if (isset($options['info_fields']) && $options['info_fields']): ?>
                    <ul class="lf-contact">
                        <?php foreach ((array) $options['info_fields'] as $info_field):
                            if ( ! isset( $info_field['icon'] ) ) {
                                $info_field['icon'] = '';
                            }
                            if ( ! ( $field_value = $listing->get_field( $info_field['show_field'] ) ) ) {
                                continue;
                            }
                            $field_value = apply_filters( 'case27\listing\preview\info_field\\' . $info_field['show_field'], $field_value, $info_field, $listing );
                            if ( is_array( $field_value ) ) {
                                $field_value = join( ', ', $field_value );
                            }
                            $GLOBALS['c27_active_shortcode_content'] = $field_value;
                            $field_content = str_replace( '[[field]]', $field_value, do_shortcode( $info_field['label'] ) );
                            ?>
                            <?php if (trim($field_value) && trim($field_content)): ?>									
								<?php if($info_field['show_field'] == 'job_phone'): ?>
									<li>
										<i class="<?php echo esc_attr( $info_field['icon'] ) ?> sm-icon"></i>                                   
										<a href="tel:<?php echo $field_value ?>" rel="nofollow" onclick="trackListing(this, '<?php echo $listing->get_name() ?> - Phone');">
											<?php echo esc_html( $field_content ) ?>
										</a>	
									</li>	
								<?php elseif($info_field['show_field'] == 'job_website'): ?>
									<li>
										<i class="<?php echo esc_attr( $info_field['icon'] ) ?> sm-icon"></i>  
										<a href="<?php echo esc_url($field_value) ?>" target="_blank" rel="nofollow" onclick="trackListing(this, '<?php echo $listing->get_name() ?> - Website');">
											Website
										</a>
									</li>
								<?php endif; ?>							
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>
					<ul class="lf-contact">
                        <?php foreach ((array) $options['info_fields'] as $info_field):
                            if ( ! isset( $info_field['icon'] ) ) {
                                $info_field['icon'] = '';
                            }
                            if ( ! ( $field_value = $listing->get_field( $info_field['show_field'] ) ) ) {
                                continue;
                            }
                            $field_value = apply_filters( 'case27\listing\preview\info_field\\' . $info_field['show_field'], $field_value, $info_field, $listing );
                            if ( is_array( $field_value ) ) {
                                $field_value = join( ', ', $field_value );
                            }
                            $GLOBALS['c27_active_shortcode_content'] = $field_value;
                            $field_content = str_replace( '[[field]]', $field_value, do_shortcode( $info_field['label'] ) );
                            ?>
                            <?php if (trim($field_value) && trim($field_content)): ?>
								<?php if($info_field['show_field'] == 'job_location'): ?>	
									<li>
										<i class="icon-location-map-1 sm-icon"></i>
										<a href="http://maps.google.com/maps?daddr=<?php echo $latitude ?>,<?php echo $longitude ?>" target="_blank" rel="nofollow" onclick="trackListing(this, '<?php echo $listing->get_name() ?> - Directions');">
											Directions
										</a>
									</li>
									<li>
										<i class="<?php echo esc_attr( $info_field['icon'] ) ?> sm-icon"></i>  
										<a href="http://maps.google.com/maps?daddr=<?php echo $latitude ?>,<?php echo $longitude ?>" target="_blank" rel="nofollow" onclick="trackListing(this, '<?php echo $listing->get_name() ?> - Suburb');">
											<?php echo $suburb ?>
										</a>
									</li>
								<?php endif; ?>								
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>					
                    <?php endif ?>
					
					<?php $footer_section_count = 0; ?>
					<?php if ($options['footer']['sections']): ?>
						<?php foreach ((array) $options['footer']['sections'] as $section): ?>

							<?php if ($section['type'] == 'categories'):
								$taxonomies = [
									'job_listing_category' => 'job_category',
									'case27_job_listing_tags' => 'job_tags',
									'region' => 'region',
								];

								$taxonomy = ! empty( $section['taxonomy'] ) ? $section['taxonomy'] : 'job_listing_category';

								if ( ! isset( $taxonomies[ $taxonomy ] ) ) {
									continue;
								}

								if ( ! $terms = $listing->get_field( $taxonomies[ $taxonomy ] ) ) {
									continue;
								}

								$footer_section_count++;
								?>
								<div class="listing-details c27-footer-section">
									<ul class="c27-listing-preview-category-list">

										<?php if ( $terms ):
											$category_count = count( $terms );
											$first_category = array_shift( $terms );
											$first_ctg = new CASE27\Classes\Term( $first_category );
											$category_names = array_map(function($category) {
												return $category->name;
											}, $terms);
											$categories_string = join('<br>', $category_names);
											?>
											<li>
												<span class="cat-icon" style="background-color: <?php echo esc_attr( $first_ctg->get_color() ) ?>;">
													<i class="<?php echo esc_attr( $first_ctg->get_icon() ) ?>"
														style="color: <?php echo esc_attr( $first_ctg->get_text_color() ) ?>"></i>
												</span>
												<span class="category-name"><?php echo esc_html( $first_ctg->get_name() ) ?></span>
											</li>
										<?php endif ?>
									</ul>

									<div class="ld-info">
										<ul>
											<?php if (isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes'): ?>
												<?php echo $quick_view_button ?>
											<?php endif ?>
											<?php if (isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes'): ?>
												<?php echo $bookmark_button ?>
											<?php endif ?>
										</ul>
									</div>
								</div>
							<?php endif ?>

							<?php if ($section['type'] == 'details' && $section['details']): $footer_section_count++; ?>
								<div class="listing-details-3 c27-footer-section">
									<ul class="details-list">
										<?php foreach ((array) $section['details'] as $detail):
											if ( ! isset( $detail['icon'] ) ) {
												$detail['icon'] = '';
											}

											if ( ! ( $detail_val = $listing->get_field( $detail['show_field'] ) ) ) {
												continue;
											}

											$detail_val = apply_filters( 'case27\listing\preview\detail\\' . $detail['show_field'], $detail_val, $detail, $listing );

											if ( is_array( $detail_val ) ) {
												$detail_val = join( ', ', $detail_val );
											}

											$GLOBALS['c27_active_shortcode_content'] = $detail_val; ?>
											<li>
												<i class="<?php echo esc_attr( $detail['icon'] ) ?>"></i>
												<span><?php echo str_replace( '[[field]]', $detail_val, do_shortcode( $detail['label'] ) ) ?></span>
											</li>
										<?php endforeach ?>
									</ul>
								</div>
							<?php endif ?>

							<?php if ($section['type'] == 'actions' || $section['type'] == 'details'): ?>
								<?php if (
									( isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes' ) ||
									( isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes' )
								 ): $footer_section_count++; ?>
									<div class="listing-details actions c27-footer-section">
										<div class="ld-info">
											<ul>
												<?php if (isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes'): ?>
													<?php echo $quick_view_button ?>
												<?php endif ?>
												<?php if (isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes'): ?>
													<?php echo $bookmark_button ?>
												<?php endif ?>
											</ul>
										</div>
									</div>
								<?php endif ?>
							<?php endif ?>
						<?php endforeach ?>
					<?php endif ?>

					<?php if ( $footer_section_count < 1 ): ?>
						<div class="c27-footer-empty"></div>
					<?php endif ?>					
					
                </div>
            <?php endif ?>

            <?php if ($options['buttons']): ?>
                <div class="lf-head">

                    <?php if ( $isPromoted ): ?>
                        <div class="lf-head-btn ad-badge">
                            <span>
                                <i class="icon-flash"></i><?php _e( 'Ad', 'my-listing' ) ?>
                            </span>
                        </div>
                    <?php endif ?>

                    <?php foreach ($options['buttons'] as $button): ?>

                        <?php if ( $button['show_field'] == '__listing_rating' && $listing_rating = CASE27_Integrations_Review::get_listing_rating_optimized($listing->get_id()) ): ?>
                            <div class="lf-head-btn listing-rating">
                                <span class="value"><?php echo esc_html( $listing_rating ) ?></span>
                                <sup class="out-of">/<?php echo CASE27_Integrations_Review::max_rating( $listing->get_id() ); ?></sup>
                            </div>
                        <?php elseif ( $button['show_field'] == 'work_hours' && $listing->get_field('work_hours') ):
                            $open_now = $listing->get_schedule()->get_open_now(); ?>
                            <div class="lf-head-btn <?php echo $open_now ? 'open' : 'close' ?>-status">
                                <span><?php echo $open_now ? __( 'Open', 'my-listing' ) : __( 'Closed', 'my-listing' ) ?></span>
                            </div>
                        <?php else:
                            if ( ! ( $button_val = $listing->get_field( $button['show_field'] ) ) ) {
                                continue;
                            }

                            $button_val = apply_filters( 'case27\listing\preview\button\\' . $info_field['show_field'], $button_val, $button, $listing );

                            if ( is_array( $button_val ) ) {
                                $button_val = join( ', ', $button_val );
                            }

                            $GLOBALS['c27_active_shortcode_content'] = $button_val;
                            $btn_content = str_replace( '[[field]]', $button_val, do_shortcode( $button['label'] ) );
                            ?>

                            <?php if ( trim( $btn_content ) ): ?>
                                <div class="lf-head-btn <?php echo has_shortcode($button['label'], '27-format') ? 'formatted' : '' ?>">
                                    <?php echo $btn_content ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>

                    <?php endforeach ?>

                </div>
            <?php endif ?>
        </a>
    </div>

    <?php ob_start() ?>
        <li class="item-preview" data-toggle="tooltip" data-placement="bottom" data-original-title="Quick view">
            <a href="#" type="button" class="c27-toggle-quick-view-modal" data-id="<?php echo esc_attr( $listing->get_id() ); ?>"><i class="material-icons">zoom_in</i></a>
        </li>
    <?php $quick_view_button = ob_get_clean() ?>

    <?php ob_start() ?>
        <li data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Bookmark">
            <a class="c27-bookmark-button <?php echo CASE27_Integrations_Bookmark::instance()->is_bookmarked($listing->get_id(), get_current_user_id()) ? 'bookmarked' : '' ?>"
               data-listing-id="<?php echo esc_attr( $listing->get_id() ) ?>" data-nonce="<?php echo esc_attr( wp_create_nonce('c27_bookmark_nonce') ) ?>">
               <i class="material-icons">favorite_border</i>
            </a>
        </li>
    <?php $bookmark_button = ob_get_clean() ?>
		
</div>
</div>