<?php get_header();

while(have_posts()): the_post();

	if (get_post_type() == 'job_listing'):		
		
		$listings_file = get_template_directory() . '/search-cache/profile-' . $post->post_name . '.txt';

		if(file_exists($listings_file)){
			$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
			echo file_get_contents($listings_file,false,$context);								
		} else {
			get_job_manager_template_part( 'content-single', 'job_listing' );
		}

	else: ?>			
	
	<?php get_template_part('partials/content', 'single') ?>

	<?php endif ?>
<?php endwhile ?>

<?php get_footer() ?>