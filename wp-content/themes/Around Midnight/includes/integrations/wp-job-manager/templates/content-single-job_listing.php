<?php global $post;

    if ( ! class_exists( 'WP_Job_Manager' ) ) {
        return;
    }

    $listing = new CASE27\Classes\Listing( $post );

    if ( ! $listing->type ) {
        return;
    }

    // Get the preview template options for the listing type of the current listing.
    $options = c27()->get_listing_type_options(
                    c27()->get_job_listing_type(get_the_ID()),
                    ['single', 'fields']
                );

    // Get the layout blocks for the single listing page.
    $layout = $options['single'];
    $fields = $options['fields'];

    // Possible cover button styles.
    $button_styles = [
        'primary' => 'button-primary',
        'secondary' => 'button-secondary',
        'outline' => 'button-outlined',
        'plain' => 'button-plain',
        'none' => 'button-plain',
    ];
	
	$categories = wp_get_post_terms( $post->ID, 'job_listing_category', array( 'orderby' => 'parent', 'order' => 'ASC' ) );
	$first_term = array_shift( $categories );
	$job_listing_category = $first_term->name;
	
	$latitude = false;
    $longitude = false;

    if ( is_numeric( $listing->get_data('geolocation_lat') ) ) {
        $latitude = $listing->get_data('geolocation_lat') + ( rand(0, 1000) / 10e6 );
    }

    if ( is_numeric( $listing->get_data('geolocation_long') ) ) {
        $longitude = $listing->get_data('geolocation_long') + ( rand( 0, 1000 ) / 10e6 );
    }

    $listing_logo = job_manager_get_resized_image( $listing->get_field( 'job_logo' ), 'medium' );
?>
<style>.c27-main-header.header.header-fixed, .overlay { background: #001d30!important; }</style>
<script>jQuery(document).ready(function() { trackProfile('<?php the_title() ?>'); });</script>
<!-- SINGLE LISTING PAGE -->
<div class="single-job-listing <?php echo ! $listing_logo ? 'listing-no-logo' : '' ?>" id="c27-single-listing">
    <input type="hidden" id="case27-post-id" value="<?php echo esc_attr( get_the_ID() ) ?>">
    <input type="hidden" id="case27-author-id" value="<?php echo esc_attr( get_the_author_meta('ID') ) ?>">

    <!-- LISTING NO COVER -->
    <?php if ($layout['cover']['type'] == 'none'): ?>
        <section class="featured-section profile-cover profile-cover-no-img">
            <div class="overlay" style="
                background-color: <?php echo esc_attr( c27()->get_setting('single_listing_cover_overlay_color', '#242429') ); ?>;
                opacity: <?php echo esc_attr( c27()->get_setting('single_listing_cover_overlay_opacity', '0.5') ); ?>;
                "></div>
    <?php endif ?>

		</section>

    <div class="profile-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div v-pre>
                        <?php if ( $listing_logo ):                            
                            ?>
                            <p class="profile-avatar" onclick="jQuery('.owl-item a').first().click();"                                
                               style="background-image: url('<?php echo esc_url( $listing->get_field( 'job_logo' ) ) ?>')"
                               data-width="<?php echo esc_attr( $width ) ?>"
                               data-height="<?php echo esc_attr( $height ) ?>"
                               >
                            </p>							
                        <?php endif ?>
                    </div>
                    <div class="profile-name" v-pre>
						<span class="category-name"><?php echo $job_listing_category ?></span>
                        <h1 class="case27-primary-text"><?php the_title() ?></h1>
                        <?php if ( $listing->get_field('job_tagline') ): ?>
                            <h2><?php echo esc_html( $listing->get_field('job_tagline') ) ?></h2>
                        <?php elseif ( $listing->get_field('job_description') ): ?>
                            <h2><?php echo c27()->the_text_excerpt( $listing->get_field('job_description'), 77 ) ?></h2>
                        <?php endif ?>
						<?php $open_now = $listing->get_schedule()->get_open_now(); ?>
						<div class="lf-head-btn <?php echo $open_now ? 'open' : 'close' ?>-status" onclick="scrollToOpenHours();">
							<span><?php echo $open_now ? __( 'Open', 'my-listing' ) : __( 'Closed', 'my-listing' ) ?></span>
						</div>
                    </div>
					
					
					<!-- COVER BUTTONS -->
					<div class="profile-cover-content reveal">
						<div class="container">
							<div class="cover-buttons">
								<ul v-pre>
									<?php if ($layout['buttons']): ?>
										<?php foreach ($layout['buttons'] as $button):
											$buttonID = uniqid() . '__cover_button';
											$button_classes = '';
											$button_style = isset($button['style']) && in_array($button['style'], array_keys($button_styles)) ? $button_styles[$button['style']] : 'button-outlined';
											if (isset($button['label']) && isset($button['icon']) && $button['icon']) {
												$button['label'] = c27()->get_icon_markup($button['icon']) . '<span class="button-label">' . $button['label'] . '</span>';
											}
											?>

											<?php if ($button['action'] == 'custom-field' && ( $meta_value = $listing->get_field( $button['custom_field'] ) ) ): ?>
												<li>
													<?php

													$meta_value = apply_filters( 'case27\listing\cover\field\\' . $button['custom_field'], $meta_value, $button, $listing );

													if ( is_array( $meta_value ) ) {
														$meta_value = join( ', ', $meta_value );
													}

													$GLOBALS['c27_active_shortcode_content'] = $meta_value;
													$btn_content = str_replace( '[[field]]', $meta_value, do_shortcode( $button['label'] ) );

													if ( has_shortcode( $button['label'], '27-format') ) {
														$button_classes.= ' formatted ';

														preg_match('/\[27-format.*type="(?<format_type>[^"]+)"/', $button['label'], $matches);

														if (isset($matches['format_type']) && $matches['format_type']) {
															$button_classes .= ' ' . $matches['format_type'] . ' ';
														}
													}
													?>
													<?php if (trim($meta_value) && trim($btn_content)): ?>
														<div class="buttons <?php echo esc_attr( $button_style ) ?> <?php echo esc_attr( $button_classes ) ?>">                                               
															<?php if($button['custom_field'] == 'job_website'): ?>
																<a href="<?php echo esc_url($meta_value) ?>" target="_blank" rel="nofollow" onclick="trackProfile('<?php the_title() ?> - Website');">
																	<i class="fa fa-globe sm-icon"></i>
																	Website
																</a>
															<?php elseif($button['custom_field'] == 'job_phone'): ?>
																<a href="tel:<?php echo $meta_value ?>" rel="nofollow" onclick="trackProfile('<?php the_title() ?> - Phone');">
																	<?php echo $btn_content; ?>
																</a>
															<?php elseif($button['custom_field'] == 'job_location'): ?>											
																<a href="http://maps.google.com/maps?daddr=<?php echo $latitude ?>,<?php echo $longitude ?>" target="_blank" rel="nofollow" onclick="trackProfile('<?php the_title() ?> - Directions');">
																	<i class="icon-location-map-1 sm-icon"></i>
																	<?php if(strlen($listing->get_field( $button['custom_field'])) > 35): ?>
																		<?php echo substr($listing->get_field( $button['custom_field']), 0, 35) . '...' ?>
																	<?php else: ?>
																		<?php echo $listing->get_field( $button['custom_field']) ?>
																	<?php endif; ?>
																</a>	
															<?php else: ?>
																<?php echo $btn_content; ?>
															<?php endif; ?>												
														</div>
													<?php endif ?>
												</li>
											<?php endif ?>

											<?php if ($button['action'] == 'display-rating' && ($listing_rating = CASE27_Integrations_Review::get_listing_rating_optimized(get_the_ID())) ): ?>
												 <li>
													 <div class="inside-rating listing-rating <?php echo esc_attr( $button_style ) ?>">
														 <span class="value"><?php echo esc_html( $listing_rating ) ?></span>
														 <sup class="out-of">/<?php echo CASE27_Integrations_Review::max_rating( get_the_ID() ); ?></sup>
													 </div>
												 </li>
											<?php endif ?>

											<?php if ($button['action'] == 'bookmark'): ?>
												<li>
													<a href="#" data-listing-id="<?php echo esc_attr( get_the_ID() ) ?>" data-nonce="<?php echo esc_attr( wp_create_nonce('c27_bookmark_nonce') ) ?>"
													   class="buttons <?php echo esc_attr( $button_style ) ?> medium bookmark c27-bookmark-button <?php echo CASE27_Integrations_Bookmark::instance()->is_bookmarked(get_the_ID(), get_current_user_id()) ? 'bookmarked' : '' ?>">
														<?php echo do_shortcode($button['label']) ?>
													</a>
												</li>
											<?php endif ?>

											<?php if ($button['action'] == 'book'): ?>
												<li>
													<a href="#book-now" class="buttons <?php echo esc_attr( $button_style ) ?> medium book-now c27-book-now">
														<?php echo do_shortcode($button['label']) ?>
													</a>
												</li>
											<?php endif ?>

											<?php if ($button['action'] == 'add-review'): ?>
											   <li>
												   <a href="#add-review" class="buttons <?php echo esc_attr( $button_style ) ?> add-review c27-add-listing-review">
													   <?php echo do_shortcode($button['label']) ?>
												   </a>
											   </li>
											<?php endif ?>

											<?php if ($button['action'] == 'share'): ?>
												<?php $links = c27('Integrations_Share')->get_links([
													'permalink' => get_permalink(),
													'image' => job_manager_get_resized_image( $listing->get_field( 'job_logo' ), 'large'),
													'title' => get_the_title(),
													'description' => get_the_content(),
													]) ?>

													<?php if ($links): ?>
														<li class="dropdown">
															<a href="#" class="buttons <?php echo esc_attr( $button_style ) ?> medium show-dropdown sn-share" type="button" id="<?php echo esc_attr( $buttonID ) ?>" data-toggle="dropdown">
																<?php echo do_shortcode( $button['label'] ) ?>
															</a>
															<ul class="i-dropdown share-options dropdown-menu" aria-labelledby="<?php echo esc_attr( $buttonID ) ?>">
																<?php foreach ($links as $link): ?>
																	<li><?php c27('Integrations_Share')->print_link( $link ) ?></li>
																<?php endforeach ?>
															</ul>
														</li>
													<?php endif ?>
											<?php endif ?>
										<?php endforeach ?>
									<?php endif ?>

									<li class="dropdown">
										<a href="#" class="buttons button-outlined medium show-dropdown c27-listing-actions" type="button" id="more-actions" data-toggle="dropdown">
											<i class="mi more_vert"></i>
										</a>
										<ul class="i-dropdown share-options dropdown-menu" aria-labelledby="more-actions">
											<?php
											if ( job_manager_user_can_edit_job( $post->ID ) && function_exists( 'wc_get_account_endpoint_url' ) ) :
												$endpoint = wc_get_account_endpoint_url( 'my-listings' );
												$edit_link = add_query_arg([
													'action' => 'edit',
													'job_id' => $post->ID
													], $endpoint);
												?>
												<li><a href="<?php echo esc_url( $edit_link ) ?>"><?php _e( 'Edit Listing', 'my-listing' ) ?></a></li>
											<?php endif ?>
											<li><a href="#" data-toggle="modal" data-target="#report-listing-modal"><?php _e( 'Report this Listing', 'my-listing' ) ?></a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>				
					
                    <div class="cover-details" v-pre>
                        <ul></ul>
                    </div>
                    <div class="profile-menu">
                        <ul role="tablist">
                            <?php $i = 0;
                            foreach ((array) $layout['menu_items'] as $key => $menu_item): $i++;
                                if (
                                    $menu_item['page'] == 'bookings' &&
                                    $menu_item['provider'] == 'timekit' &&
                                    ! $listing->get_field( $menu_item['field'] )
                                ) { continue; }

                                ?><li class="<?php echo ($i == 1) ? 'active' : '' ?>">
                                    <a href="<?php echo "#_tab_{$i}" ?>" aria-controls="<?php echo esc_attr( "_tab_{$i}" ) ?>" data-section-id="<?php echo esc_attr( "_tab_{$i}" ) ?>"
                                       role="tab" class="tab-reveal-switch <?php echo esc_attr( "toggle-tab-type-{$menu_item['page']}" ) ?>">
                                        <?php echo esc_html( $menu_item['label'] ) ?>

                                        <?php if ($menu_item['page'] == 'comments'): ?>
                                            <span class="items-counter"><?php echo get_comments_number() ?></span>
                                        <?php endif ?>

                                        <?php if (in_array($menu_item['page'], ['related_listings', 'store'])):
                                            $vue_data_keys = ['related_listings' => 'related_listings', 'store' => 'products'];
                                            ?>
                                            <span class="items-counter" v-if="<?php echo esc_attr( $vue_data_keys[$menu_item['page']] ) ?>['_tab_<?php echo esc_attr( $i ) ?>'].loaded" v-cloak>
                                                {{ <?php echo $vue_data_keys[$menu_item['page']] ?>['_tab_<?php echo $i ?>'].count }}
                                            </span>
                                            <span v-else class="c27-tab-spinner">
                                                <i class="fa fa-circle-o-notch fa-spin"></i>
                                            </span>
                                        <?php endif ?>
                                    </a>
                                </li><?php
                            endforeach; ?>
                            <div id="border-bottom"></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <?php $i = 0; ?>
        <?php foreach ((array) $layout['menu_items'] as $key => $menu_item): $i++; ?>
            <section class="tab-pane profile-body <?php echo ($i == 1) ? 'active' : '' ?> <?php echo esc_attr( "tab-type-{$menu_item['page']}" ) ?>" id="<?php echo esc_attr( "_tab_{$i}" ) ?>" role="tabpanel">

                <?php if ($menu_item['page'] == 'main' || $menu_item['page'] == 'custom'): ?>
                    <div class="container" v-pre>
                        <div class="row grid reveal">
						
							<?php foreach ($menu_item['layout'] as $block):
							
								$block_wrapper_class = 'col-md-12 col-sm-12 col-xs-12 grid-item';

                                if (
                                    $listing->type && ! empty( $block['show_field'] ) &&
                                    $listing->get_field( $block['show_field'] ) &&
                                    $listing->type->get_field( $block['show_field'] )
                                ) {
                                    $field = $listing->type->get_field( $block['show_field'] );
                                } else {
                                    $field = null;
                                }
								
								// Gallery Block.
                                if ( $block['type'] == 'gallery' && ( $gallery_items = (array) $listing->get_field( $block['show_field'] ) ) ) {																											
                                    $gallery_type = 'carousel';
                                    foreach ((array) $block['options'] as $option) {
                                        if ($option['name'] == 'gallery_type') $gallery_type = $option['value'];
                                    }

                                    if ( array_filter( $gallery_items ) ) {
                                        c27()->get_section('gallery-block', [
                                            'ref' => 'single-listing',
                                            'icon' => 'material-icons://insert_photo',
                                            'title' => $block['title'],
                                            'gallery_type' => $gallery_type,
                                            'wrapper_class' => $block_wrapper_class,
                                            'gallery_items' => array_filter( $gallery_items ),
                                            'gallery_item_interface' => 'CASE27_JOB_MANAGER_ARRAY',
                                            ]);
                                    }
                                }								
								
								do_action( "case27/listing/blocks/{$block['type']}", $block );

                            endforeach ?>

                            <?php foreach ($menu_item['layout'] as $block):
							
                                $block_wrapper_class = 'col-md-12 col-sm-12 col-xs-12 grid-item';

                                if (
                                    $listing->type && ! empty( $block['show_field'] ) &&
                                    $listing->get_field( $block['show_field'] ) &&
                                    $listing->type->get_field( $block['show_field'] )
                                ) {
                                    $field = $listing->type->get_field( $block['show_field'] );
                                } else {
                                    $field = null;
                                }

                                // Text Block.
                                if ( $block['type'] == 'text' && ( $block_content = $listing->get_field( $block['show_field'] ) ) ) {
                                    c27()->get_section('content-block', [
                                        'ref' => 'single-listing',
                                        'icon' => 'material-icons://view_headline',
                                        'title' => $block['title'],
                                        'content' => $block_content,
                                        'wrapper_class' => $block_wrapper_class,
                                        'escape_html' => $field && $field['type'] !== 'wp-editor',
                                        ]);
                                }

                                // Categories Block.
                                /*if ( $block['type'] == 'categories' && ( $terms = $listing->get_field( 'job_category' ) ) ) {
                                    c27()->get_section('listing-categories-block', [
                                        'ref' => 'single-listing',
                                        'icon' => 'material-icons://view_module',
                                        'title' => $block['title'],
                                        'terms' => $terms,
                                        'wrapper_class' => $block_wrapper_class,
                                        ]);
                                }*/

                                // Location Block.
                                if ( $block['type'] == 'location' && ( $block_location = $listing->get_field( $block['show_field'] ) ) ) {
                                    $listing_logo = c27()->image('27.jpg');
                                    if ( $listing->get_field( 'job_logo' ) ) {
                                        $listing_logo = job_manager_get_resized_image( $listing->get_field( 'job_logo' ), 'thumbnail' );
                                    }

                                    $location_arr = [
                                        'address' => $block_location,
                                        'marker_image' => ['url' => $listing_logo],
                                    ];

                                    if ( $block['show_field'] == 'job_location' && ( $lat = $listing->get_data('geolocation_lat') ) && ( $lng = $listing->get_data('geolocation_long') ) ) {
                                        $location_arr = [
                                            'marker_lat' => $lat,
                                            'marker_lng' => $lng,
                                            'marker_image' => ['url' => $listing_logo],
                                        ];
                                    }

                                    $map_skin = 'skin1';
                                    if ( ! empty( $block['options'] ) ) {
                                        foreach ((array) $block['options'] as $option) {
                                            if ($option['name'] == 'map_skin') $map_skin = $option['value'];
                                        }
                                    }

                                    c27()->get_section('map', [
                                        'ref' => 'single-listing',
                                        'icon' => 'material-icons://map',
                                        'title' => $block['title'],
                                        'wrapper_class' => $block_wrapper_class,
                                        'template' => 'block',
                                        'options' => [
                                            'locations' => [ $location_arr ],
                                            'zoom' => 15,
                                            'draggable' => false,
                                            'skin' => $map_skin,
                                        ],
                                    ]);
                                }					
								
                                // Table Block.
                                if ( $block['type'] == 'table' && count( $rows ) ) {
                                    c27()->get_section('table-block', [
                                        'ref' => 'single-listing',
                                        'icon' => 'material-icons://view_module',
                                        'title' => $block['title'],
                                        'rows' => $rows,
                                        'wrapper_class' => $block_wrapper_class,
                                        ]);
                                }


                                // Details Block.
                                if ( $block['type'] == 'details' && count( $rows ) ) {
                                    c27()->get_section('list-block', [
                                        'ref' => 'single-listing',
                                        'icon' => 'material-icons://view_module',
                                        'title' => $block['title'],
                                        'item_interface' => 'CASE27_DETAILS_ARRAY',
                                        'items' => $rows,
                                        'wrapper_class' => $block_wrapper_class,
                                        ]);
                                }

                                // Work Hours Block.
                                if ($block['type'] == 'work_hours' && ( $work_hours = $listing->get_field( 'work_hours' ) ) ) {
                                    c27()->get_section('work-hours-block', [
                                        'wrapper_class' => $block_wrapper_class . ' open-now sl-zindex',
                                        'ref' => 'single-listing',
                                        'title' => $block['title'],
                                        'icon' => 'material-icons://alarm',
                                        'hours' => (array) $work_hours,
                                    ]);
                                }

                                // Social Networks (Links) Block.
                                /*if ( $block['type'] == 'social_networks' && ( $links = $listing->get_field('links') ) ) {
                                    c27()->get_section('list-block', [
                                        'ref' => 'single-listing',
                                        'icon' => 'material-icons://view_module',
                                        'title' => $block['title'],
                                        'item_interface' => 'CASE27_LINK_ARRAY',
                                        'items' => array_filter( array_map(function( $link ) {
                                            if ( ! is_array( $link ) || empty( $link['network'] ) || empty( $link['url'] ) ) {
                                                return false;
                                            }

                                            return ['title' => $link['network'], 'content' => $link['url']];
                                            }, (array) $links) ),
                                        'wrapper_class' => $block_wrapper_class,
                                    ]);
                                }*/

                                do_action( "case27/listing/blocks/{$block['type']}", $block );

                            endforeach ?>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($menu_item['page'] == 'comments'): ?>
                    <div v-pre>
                        <?php $GLOBALS['case27_reviews_allow_rating'] = $listing->type->is_rating_enabled() ?>
                        <?php comments_template() ?>
                    </div>
                <?php endif ?>

            </section>
        <?php endforeach; ?>
    </div>

    <?php c27()->get_partial('report-modal', ['listing' => $post]) ?>
</div>