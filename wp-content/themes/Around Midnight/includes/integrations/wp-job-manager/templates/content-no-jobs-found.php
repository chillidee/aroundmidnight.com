<div class="no-results-wrapper">
	<i class="no-results-icon material-icons">error_outline</i>
	<li class="no_job_listings_found"><?php _e( 'There are no listings matching your search.', 'my-listing' ) ?></li>
		
	<?php
	$args = array(
		'post_type' 		=> 'page',
		'posts_per_page' 	=> -1,
		'post_status'   	=> 'publish',
		'meta_query' => array(
			array(
			'key' 		=> 'is_landing_page',
			'value' 	=> 1,
			'compare' 	=> '='
			)
		)	
	);
	$landing_pages = new WP_Query( $args ); 
	
	// It shows landing pages as suggestions
	if ( $landing_pages->have_posts() ) {		  
			  
		$form_data = $_POST['form_data'];
		$categories = array_filter( array_map('sanitize_text_field', array_map('stripslashes', (array) $form_data['job_category'] ) ) );
		
		$main_cities = array();
		$main_cities_categories = array();
		
		// Gather data for suggestions
		while ( $landing_pages->have_posts() ) {		
		
			$landing_pages->the_post();
			$landing_page_category = array_filter( array_map('sanitize_text_field', array_map('stripslashes', (array) get_post_meta(get_the_ID(), 'search_category', true) ) ) );
			$landing_page_city = get_post_meta(get_the_ID(), 'search_city', true);
			
			// If the user wasn't looking for specific category, it shows only main cities
			if(count($categories) == 0) {				
				if(count($landing_page_category) == 0) {					
					$main_cities[$landing_page_city] = '<a href="'.get_permalink().'" style="color:#d70075"><strong>' . $landing_page_city . '</strong></a>';				
				}				
			} else { // Otherwise, it shows main cities filtered by category				
						
				if(count($landing_page_category) > 0 && in_array($landing_page_category['slug'], $categories)) {
					$main_cities_categories[$landing_page_category['slug'].'_'.$landing_page_city] = '<a href="'.get_permalink().'" style="color:#d70075"><strong>' . $landing_page_city . '</strong></a>';
				}
				
			}
		}
		
		// Assembles and output suggestions tidily
		if (count($categories) == 0 && count($main_cities) > 0) {
			
			ksort($main_cities);
			$suggestions_msg = '';
			
			foreach ($main_cities as $key => $value) {
				$suggestions_msg .= $value . ' | ';
			}
			
			echo '<br/><br/>';
			echo '<p class="text-center no_job_listings_found"><strong>But you may look at these places near you:</strong></p>';
			echo '<p class="text-center no_job_listings_found">'.substr($suggestions_msg, 0, -3).'</p>';
			
		} else if (count($categories) > 0 && count($main_cities_categories) > 0) {
			
			ksort($main_cities_categories);
			$suggestions_msg = '';
			
			foreach($categories as $category) {
				
				echo '<br/><br/>';
				if (count($categories) == 1) {
					echo '<p class="text-center no_job_listings_found"><strong>But there are other '.ucwords(str_replace('-', ' ', $category)).' in Australia that you might like to try:</strong></p>';
				} else {
					echo '<p class="text-center no_job_listings_found"><strong>There are other '.ucwords(str_replace('-', ' ', $category)).' in Australia that you might like to try:</strong></p>';
				}
				
				$suggestions_msg = '';
				
				foreach($main_cities_categories as $key => $value) {
					if(substr($key, 0, strlen($category)) == $category) {
						$suggestions_msg .= $value . ' | ';
					}
				}
				echo '<p class="text-center no_job_listings_found">'.substr($suggestions_msg, 0, -3).'</p>';				
			}
			
			$all_categories = (array) get_terms([
				'taxonomy' => 'job_listing_category',
				'hide_empty' => false,        
				'orderby' => 'title',
			]);
			
			$suggestions_msg = '';
			
			foreach ( $all_categories as $category) {	
				$term = new CASE27\Classes\Term( $category );
				$value = '<a href="explore/?search_location=&job_category%5B%5D='.$term->get_slug().'" style="color:#d70075"><strong>' . $term->get_name() . '</strong></a>';
				$suggestions_msg .= $value . ' | ';				
			}
			
			if($suggestions_msg) {
				echo '<br/><br/>';
				echo '<p class="text-center no_job_listings_found"><strong>Or you may like to try other categories in Australia:</strong></p>';
				echo '<p class="text-center no_job_listings_found">'.substr($suggestions_msg, 0, -3).'</p>';
			}
		}
		
	} else {
	  // no posts found
	}
		
	?>
	
</div>