<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class CASE27_Elementor_Widget_Trending_Posts extends Widget_Base {

	public function get_name() {
		return 'trending-posts-widget';
	}

	public function get_title() {
		return '<strong>27</strong> > Trending Posts';
	}

	public function get_icon() {
		// Icon name from the Elementor font file, as per http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-post-list';
	}

	protected function _register_controls() {
		
		$this->start_controls_section(
			'section_content_block',
			[
				'label' => 'Settings',
			]
		);

		$this->add_control(
			'the_title',
			[
				'label' => 'Title',
				'type' => Controls_Manager::TEXT,
				'default' => '',
			]
		);

		$this->end_controls_section();
	}


	protected function render( $instance = [] ) {

		$args = array('post_type'      	=> 'post',
  				  'posts_per_page' 	=> 5,
  				  'post_status'    	=> 'publish',
				  'meta_key' 		=> 'post_views_count',
				  'orderby' 		=> 'meta_value_num',
				  'order' 			=> 'DESC');  
	  
		$trending_posts = new WP_Query($args);  
	  
		$html = '<ul>';
	  
		while ($trending_posts->have_posts()) {
			$trending_posts->the_post(); 
			$html .= '<li><a href="' . get_permalink() . '">' . the_title() . '</a></li>';  
		}
	  
		$html .= '</ul>';
	  
		echo $html;
	}

	protected function content_template() {}

	public function render_plain_content( $instance = [] ) {}

}

Plugin::instance()->widgets_manager->register_widget_type( new CASE27_Elementor_Widget_Trending_Posts() );
