<?php

namespace CASE27\Classes\Conditions;

class Conditions {
	private $field, $conditions, $listing;

	public function __construct( $field, $listing = null ) {
		$this->field = $field;
		$this->conditions = ! empty( $field['conditions'] ) ? $field['conditions'] : [];
		$this->conditional_logic = isset( $field['conditional_logic'] ) ? $field['conditional_logic'] : false;
		$this->listing = $listing;
	}

	public function passes() {
		$results = [];

		// If there's no conditional logic, show the field.
		if ( ! $this->conditional_logic ) {
			return true;
		}

		// Title and Description need to always be visible.
		if ( in_array( $this->field['slug'], [ 'job_title', 'job_description' ] ) ) {
			return true;
		}

		$this->conditions = array_filter( $this->conditions );

		// Return true if there isn't any condition set.
		if ( empty( $this->conditions ) ) {
			return true;
		}

		// Loop through the condition blocks.
		// First level items consists of arrays related as "OR".
		// Second level items consists of conditions related as "AND".
		// dump( sprintf( 'Looping through %s condition groups...', $this->field['slug'] ) );
		foreach ( $this->conditions as $conditionGroup ) {
			if ( empty( $conditionGroup ) ) {
				continue;
			}

			foreach ( $conditionGroup as $condition ) {
				if ( $condition['key'] == '__listing_package' ) {
					if ( ! ( $package_id = $this->get_package_id() ) ) {
						// dump( 'Condition failed (package id not found).' );
						$results[] = false;
						continue(2);
					}

					if ( ! $this->compare( $condition, $package_id ) ) {
						// dump( 'Condition failed.', $condition );
						$results[] = false;
						continue(2);
					}

					// dump( 'Condition passed.' );
				}
			}

			$results[] = true;
		}

		// Return true if any of the condition groups is true.
		return in_array( true, $results );
	}

	public function compare( $condition, $value ) {
		if ( $condition['compare'] == '==' ) {
			return $condition['value'] == $value;
		}

		if ( $condition['compare'] == '!=' ) {
			return $condition['value'] != $value;
		}

		return false;
	}

	public function get_package_id() {
		// Determine the paid package ID.
		if ( $this->listing ) {
			return $this->listing->_package_id;
		} elseif ( ! empty( $_POST['job_package'] ) ) {
			return absint( is_numeric( $_POST['job_package'] ) ? $_POST['job_package'] : substr( $_POST['job_package'], 5 ) );
		} elseif ( ! empty( $_COOKIE['chosen_package_id'] ) ) {
			return absint( $_COOKIE['chosen_package_id'] );
		}

		return false;
	}
}