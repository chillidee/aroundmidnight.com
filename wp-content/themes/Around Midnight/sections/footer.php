<?php
	$data = c27()->merge_options([
			'footer_text'      => c27()->get_setting('footer_text', 'Copyright {{year}} Around Midnight | All Rights Reserved.'),
			'show_widgets'     => c27()->get_setting('footer_show_widgets', true),
			'show_footer_menu' => c27()->get_setting('footer_show_menu', true),
		], $data);
?>

<footer class="footer">
	<div class="container">		
		<div class="row">
			<div class="col-md-12 reveal">
				<div class="footer-bottom">
					<div class="row">
						<div class="col-md-4 col-sm-3 col-xs-12 copyright">						
							<ul>
								<li>									
									<label>Around Midnight</label>						
								</li>								
								<?php 
								$menu = wp_get_nav_menu_object('footer-about-us');
								$menuitems = wp_get_nav_menu_items($menu->term_id);																										
								?>
								<?php foreach( $menuitems as $item ): ?>									
								<li>	
									<div class="buttons small button-plain ">
										<a href="<?php echo $item->url ?>">											
											<span class="button-label"><?php echo $item->title ?></span>
										</a>
									</div>
								</li>	
								<?php endforeach; ?>
							</ul>					
						</div>
						<div class="col-md-4 col-sm-3 col-xs-12 copyright">						
							<ul>
								<li>									
									<label>For Users</label>						
								</li>								
								<?php 
								$menu = wp_get_nav_menu_object('footer-users');
								$menuitems = wp_get_nav_menu_items($menu->term_id);																										
								?>
								<?php foreach( $menuitems as $item ): ?>									
								<li>	
									<div class="buttons small button-plain ">
										<a href="<?php echo $item->url ?>">											
											<span class="button-label"><?php echo $item->title ?></span>
										</a>
									</div>
								</li>	
								<?php endforeach; ?>
							</ul>					
						</div>
						<div class="col-md-4 col-sm-3 col-xs-12 copyright">						
							<ul>
								<li>									
									<label>For Escorts / Businesses</label>						
								</li>
								<?php 
								$menu = wp_get_nav_menu_object('footer-business');
								$menuitems = wp_get_nav_menu_items($menu->term_id);																										
								?>
								<?php foreach( $menuitems as $item ): ?>									
								<li>	
									<div class="buttons small button-plain ">
										<a href="<?php echo $item->url ?>">											
											<span class="button-label"><?php echo $item->title ?></span>
										</a>
									</div>
								</li>	
								<?php endforeach; ?>
							</ul>					
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer-splitter">
						</div>	
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 copyright">
							<a href="/terms-and-conditions" target="_blank" style="margin-right: 20px">Terms &amp; Conditions</a>
							<a href="/sitemap" target="_blank" style="margin-right: 20px">Sitemap</a>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 copyright">
							<p><?php echo str_replace( '{{year}}', date('Y'), 'Copyright &copy; {{year}} Around Midnight | All Rights Reserved.' ) ?></p>							
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 social-links">
						<?php wp_nav_menu([
								'theme_location' => 'footer',
								'container' => false,
								'menu_class' => 'main-menu',
								'items_wrap' => '<ul id="%1$s" class="%2$s social-nav">%3$s</ul>'
								]); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
