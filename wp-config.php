<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'admin_am_staging');

/** MySQL database username */
define('DB_USER', 'admin_am_staging');

/** MySQL database password */
define('DB_PASSWORD', 'cVCwciRoxS');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OA$7#)-xUmPW%-j).~UN86Fu@0y0l~tOpq %SxM-}CUTJ7tfV}nnxD<JBZ6nRr-9');
define('SECURE_AUTH_KEY',  '-46scL>V4;5zLw|.2[(.1N3)7l{Ycj7wM5.88YBW0fl*S]PKw)N_4I,6yO4H=%Q&');
define('LOGGED_IN_KEY',    'C&$HSV|AyW*gmXpA{:][,Tdm8r1^AdI`,:JmcDIqFj~+HB`)KfD2J`Q7%%CJk:L8');
define('NONCE_KEY',        'd9vkQ(!NKrMvUkbKINg=PS +(9[,Fe_E4~WN[{$]}[qC=W;^(f98kZ,D5n6,K}g|');
define('AUTH_SALT',        ',3enP^S@]PmGMX-E$<a/IS[i1~%`%?%5<A>stk3$Qtn(VbG4o#_oB:{uKW( 5:aq');
define('SECURE_AUTH_SALT', 'W)k882Y*10$mXSd[>h|6xC|f8KkVIl:z3*4H2Q44?(of3i4(=[q6su=>-&@d Mx2');
define('LOGGED_IN_SALT',   'l*;q:R9J,dZ#XhZ6iK/>ktpB$r;J>Qx_@^AF8bY_F)CVMqBHJm[[p:^I3&{P[3:q');
define('NONCE_SALT',       '-S_F B]k=UY=G8@WYn*%h!{euuuaBV30o~hRO3<H[9m`AwU;~d|kEb9I~y~FOfuf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/* Increase PHP Memory to 64MB */
define( 'WP_MEMORY_LIMIT', '64M' );

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('ALTERNATE_WP_CRON', true);